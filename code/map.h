#ifndef MAP_H
#define MAP_H

#include "util.h"
#include "cell.h"

#include <vector>
#include <cstdlib>
#include <iostream>

namespace vi
{
    class Map
    {
        public:
            Map(unsigned int width, unsigned int height);
            ~Map();

            void draw(sf::RenderWindow& window);
            void turnTile(const sf::Vector2i mousePos);

        private:
            unsigned int mWidth;
            unsigned int mHeight;
            unsigned int mTreasureX;
            unsigned int mTreasureY;

            unsigned int mTries;
            sf::Text mTriesText;

            std::vector< std::vector<vi::Cell> > mMap;
    };

    ////////////////////////////////////////////

    namespace
    {
        unsigned int WinW;
        unsigned int WinH;
        sf::Vector2f CellSize;
        float CellOutlineThickness;
        sf::Font Mono;

        sf::Texture Unknown;
        sf::Texture Left;
        sf::Texture Right;
        sf::Texture Up;
        sf::Texture Down;
        sf::Texture Treasure;

        const sf::Texture arrowShape(const sf::Vector2f size);
    }
    void init(unsigned int winW, unsigned int winH);

    namespace
    {
        const sf::Texture arrowShape(const sf::Vector2f size)
        {
            sf::VertexArray array(sf::TrianglesStrip, 0);
            array.append(sf::Vertex(sf::Vector2f( (5 * size.x / 6), (1 * size.y / 4)), sf::Color::Red)); // Starboard
            array.append(sf::Vertex(sf::Vector2f( (1 * size.x / 6), (1 * size.y / 2)), sf::Color::Red)); // Bow
            array.append(sf::Vertex(sf::Vector2f( (4 * size.x / 6), (1 * size.y / 2)), sf::Color::Red)); // Aft
            array.append(sf::Vertex(sf::Vector2f( (5 * size.x / 6), (3 * size.y / 4)), sf::Color::Red)); // Port

            sf::RenderTexture renderTexture;
            renderTexture.create(size.x, size.y);
            renderTexture.clear(sf::Color::Transparent);
            renderTexture.draw(array);
            renderTexture.display();
            return renderTexture.getTexture();
        }
    }
}

#endif // MAP_H
