#ifndef CELL_H
#define CELL_H

#include <SFML/Graphics.hpp>

namespace vi
{

    class Cell
    {
        public:
            Cell();
            Cell(const sf::Vector2f pos, const sf::Texture& texture);

            void setPosition(const sf::Vector2f pos);
            void turnTile(const sf::Texture& texture);
            const sf::Sprite getSprite() const;
            const bool getTurned() const;

        private:
            sf::Sprite mSprite;
            bool mTurned;
    };
}

#endif // CELL_H
