#include "map.h"
#include "cell.h"

#include <SFML/Graphics.hpp>

#include <cstdlib>
#include <ctime>

int main()
{
    srand(time(NULL));
    sf::RenderWindow Window(sf::VideoMode(800, 600, 32), "SFML Template");
    vi::init(Window.getSize().x, Window.getSize().y);

    vi::Map map(12, 12);

    while (Window.isOpen())
    {
        sf::Event Event;
        while (Window.pollEvent(Event))
        {
            switch(Event.type)
            {
                case sf::Event::Closed:
                    Window.close();
                    break;
                case sf::Event::KeyPressed:
                    switch(Event.key.code)
                    {
                        case sf::Keyboard::Escape:
                            Window.close();
                            break;
                        case sf::Keyboard::F5:
                            map = vi::Map(12,12);
                        default:
                            break;
                    }
                    break;
                case sf::Event::MouseButtonPressed:
                    switch(Event.mouseButton.button)
                    {
                        case sf::Mouse::Left:
                            map.turnTile(sf::Mouse::getPosition(Window));
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }
        Window.clear(sf::Color(200,200,200));
        map.draw(Window);
        Window.display();
    }

    return 0;
}
