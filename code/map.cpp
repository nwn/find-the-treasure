#include "map.h"

vi::Map::Map(unsigned int width, unsigned int height):
    mWidth(width),
    mHeight(height)
{
    mTreasureX = rand() % mWidth;
    mTreasureY = rand() % mHeight;

    sf::Vector2f mapSize((mWidth * CellSize.x) + ((mWidth + 1) * (CellOutlineThickness * 4)), (mHeight * CellSize.y) + ((mHeight + 1) * (CellOutlineThickness * 4)));
    sf::Vector2f offset((WinW - mapSize.x) / 2, (WinH - mapSize.x) / 2);

    for(unsigned int i = 0; i < mHeight; ++i)
    {
        mMap.push_back({});

        for(unsigned int j = 0; j < mWidth; ++j)
        {
            mMap[i].emplace_back(sf::Vector2f(offset.x + (CellSize.x * j) + ((j + 1) * (CellOutlineThickness * 4)),
                                              offset.y + (CellSize.y * i) + ((i + 1) * (CellOutlineThickness * 4))),
                                 Unknown);
        }
    }

    mTries = 0;
    mTriesText = sf::Text("", Mono, 16);
    mTriesText.setPosition(10,10);
}

vi::Map::~Map()
{
    for(unsigned int i = 0; i < mMap.size(); ++i)
    {
        mMap[i].clear();
    }
    mMap.clear();
}

void vi::Map::draw(sf::RenderWindow& window)
{
    for(unsigned int i = 0; i < mMap.size(); ++i)
    {
        for(unsigned int j = 0; j < mMap[i].size(); ++j)
        {
            window.draw(mMap[i][j].getSprite());
        }
    }
    mTriesText.setString("Tries: " + vi::util::toString(mTries));
    window.draw(mTriesText);
}

void vi::Map::turnTile(const sf::Vector2i mousePos)
{
    bool HClue = rand() % 2; // Determines whether to give a horizontal or vertical clue
    for(unsigned int i = 0; i < mMap.size(); ++i)
    {
        for(unsigned int j = 0; j < mMap[i].size(); ++j)
        {
            if(mMap[i][j].getSprite().getGlobalBounds().contains(mousePos.x, mousePos.y))
            {
                if(!mMap[i][j].getTurned())
                {
                    mTries++;
                    if(i == mTreasureY && j == mTreasureX) // If treasure was chosen
                    {
                        mMap[i][j].turnTile(Treasure);
                    }
                    else if((HClue == true || i == mTreasureY) && j != mTreasureX) // Show left/right arrow
                    {
                        if(j < mTreasureX)
                            mMap[i][j].turnTile(Right);
                        else
                            mMap[i][j].turnTile(Left);
                    }
                    else                                                           // Show up/down arrow
                    {
                        if(i < mTreasureY)
                            mMap[i][j].turnTile(Down);
                        else
                            mMap[i][j].turnTile(Up);
                    }
                }
                return;
            }
        }
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void vi::init(unsigned int winW, unsigned int winH)
{
    WinW = winW;
    WinH = winH;

    if(!Mono.loadFromFile("data/mono.ttf"))
        std::cerr << "Error: cannot load font file" << std::endl;

    CellSize = sf::Vector2f(40,40);
    CellOutlineThickness = 1;

    sf::RectangleShape box(CellSize);
    box.setOutlineThickness(CellOutlineThickness);
    box.setOutlineColor(sf::Color::Black);
    box.setPosition(box.getOutlineThickness(),box.getOutlineThickness());

    sf::RenderTexture RenderTex;
    RenderTex.create(box.getGlobalBounds().width, box.getGlobalBounds().height);
    RenderTex.clear(sf::Color::Transparent);

    sf::Texture arrowTexture(arrowShape(CellSize));
    sf::Sprite arrowSprite(arrowTexture);
    arrowSprite.setOrigin(arrowSprite.getGlobalBounds().width / 2, arrowSprite.getGlobalBounds().height / 2);
    arrowSprite.setPosition((CellSize.x / 2) + CellOutlineThickness, (CellSize.y / 2) + CellOutlineThickness);

    {
        box.setFillColor(sf::Color(128, 128, 128));
        RenderTex.clear(sf::Color::Transparent);
        RenderTex.draw(box);
        RenderTex.display();
        Unknown = RenderTex.getTexture();
    }
    {
        arrowSprite.setRotation(0.f);
        RenderTex.clear(sf::Color::Transparent);
        RenderTex.draw(sf::Sprite(Unknown));
        RenderTex.draw(arrowSprite);
        RenderTex.display();
        Left = RenderTex.getTexture();
    }
    {
        arrowSprite.setRotation(180.f);
        RenderTex.clear(sf::Color::Transparent);
        RenderTex.draw(sf::Sprite(Unknown));
        RenderTex.draw(arrowSprite);
        RenderTex.display();
        Right = RenderTex.getTexture();
    }
    {
        arrowSprite.setRotation(90.f);
        RenderTex.clear(sf::Color::Transparent);
        RenderTex.draw(sf::Sprite(Unknown));
        RenderTex.draw(arrowSprite);
        RenderTex.display();
        Up = RenderTex.getTexture();
    }
    {
        arrowSprite.setRotation(270.f);
        RenderTex.clear(sf::Color::Transparent);
        RenderTex.draw(sf::Sprite(Unknown));
        RenderTex.draw(arrowSprite);
        RenderTex.display();
        Down = RenderTex.getTexture();
    }
    {
        box.setFillColor(sf::Color::Yellow);
        RenderTex.clear(sf::Color::Transparent);
        RenderTex.draw(box);
        RenderTex.display();
        Treasure = RenderTex.getTexture();
    }
}
