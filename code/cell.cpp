#include "cell.h"

vi::Cell::Cell():
    mTurned(false)
{

}

vi::Cell::Cell(const sf::Vector2f pos, const sf::Texture& texture)
{
    mSprite.setPosition(pos);
    mSprite.setTexture(texture);
}

void vi::Cell::setPosition(const sf::Vector2f pos)
{
    mSprite.setPosition(pos);
}


void vi::Cell::turnTile(const sf::Texture& texture)
{
    mTurned = true;
    mSprite.setTexture(texture);
}

const sf::Sprite vi::Cell::getSprite() const
{
    return mSprite;
}

const bool vi::Cell::getTurned() const
{
    return mTurned;
}
